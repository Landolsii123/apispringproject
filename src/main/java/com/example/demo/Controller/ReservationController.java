package com.example.demo.Controller;

import com.example.demo.Dao.AnnonceRepository;
import com.example.demo.Dao.ClientRepository;
import com.example.demo.Dao.ReservationRepository;
import com.example.demo.Model.Reservation;
import com.example.demo.Model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/Reservation")
public class ReservationController {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AnnonceRepository annonceRepository;

    @GetMapping("/all")
    public List<Reservation> findAll(){
        return reservationRepository.findAll();
    }

    @PostMapping("/add/{idC}/{idA}")
    public Reservation addReservation(@RequestBody Reservation r, @PathVariable String idC,@PathVariable String idA){
        r.setClient(clientRepository.find_id(idC));
        r.setAnnonce(annonceRepository.find_id(idA));
        r.setEtat("En attente");
        r.setResponse("0");
        r.setNotif("1");
        return reservationRepository.save(r);
    }

    @PutMapping("/update/{id}/{idC}/{idA}")
    public Reservation updateReservation(@RequestBody Reservation r, @PathVariable String id,  @PathVariable String idC,@PathVariable String idA){
        r.setId(id);
        r.setEtat(r.getEtat());
        r.setNotif(r.getNotif());
        r.setResponse(r.getResponse());
        r.setClient(clientRepository.find_id(idC));
        r.setAnnonce(annonceRepository.find_id(idA));
        return reservationRepository.save(r);
    }

    @DeleteMapping("/delete/{id}")
    public Response deteleReservation (@PathVariable String id ){
        Response rs= new Response();
        try {
            reservationRepository.deleteById(id);
            rs.setState("okay");
            return rs;
        }catch(Exception e){
            rs.setState("non");
            return rs;
        }
    }

    @GetMapping("/one/{id}")
    public Reservation getOne(@PathVariable String id){
        return reservationRepository.find_id(id);
    }

    @GetMapping("/allreservation/{id}")
    public List<Reservation> allReservation(@PathVariable String id){
        return reservationRepository.findbyClient(id);
    }

    @GetMapping("/setResponse/{idReservation}/{response}")
    public Reservation changeEtat(@PathVariable String idReservation, @PathVariable String response){
        Reservation reservation=reservationRepository.find_id(idReservation);
            reservation.setEtat(response);
            reservation.setResponse("1");
            reservation.setNotif(reservation.getNotif());

        return reservationRepository.save(reservation);
    }

    @GetMapping("/setNotif/{idReservation}")
    public Reservation changeNotif(@PathVariable String idReservation){
        Reservation reservation=reservationRepository.find_id(idReservation);

        reservation.setNotif("0");

        return reservationRepository.save(reservation);
    }
    @GetMapping("/allNotif")
    public List<Reservation> reservations (){
        List<Reservation> reservationList= new ArrayList<>();
        for(Reservation reservation: reservationRepository.findAll()){
            if (reservation.getNotif().equals("1")){
                reservationList.add(reservation);
            }
        }
        return reservationList;
    }
}
