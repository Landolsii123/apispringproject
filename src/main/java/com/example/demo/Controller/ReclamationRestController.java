package com.example.demo.Controller;


import com.example.demo.Dao.ClientRepository;
import com.example.demo.Dao.ReclamationRespository;
import com.example.demo.Model.Mail;
import com.example.demo.Model.Reclamation;
import com.example.demo.Model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Reclamation")
@CrossOrigin("*")
public class ReclamationRestController {
    @Autowired
    private ReclamationRespository reclamationRespository;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private MailService mailService;


    @GetMapping("/all")
    public List<Reclamation> findAll(){
        return reclamationRespository.findAll();
    }

    @PostMapping("/add/{idC}")
    public Reclamation addReclamation(@RequestBody Reclamation c, @PathVariable String idC){
        c.setResponded("0");
        c.setClient(clientRepository.find_id(idC));
        return reclamationRespository.save(c);
    }

    @PutMapping("/update/{id}/{idC}")

    public Reclamation updateReclamation(@RequestBody Reclamation c,@PathVariable String id, @PathVariable String idC){
        c.setId(id);
        c.setClient(clientRepository.find_id(idC));
        return reclamationRespository.save(c);
    }

    @DeleteMapping("/delete/{id}")

    public Response deleteReclamation (@PathVariable String id){
        Response rs=new Response();
        try{
            reclamationRespository.deleteById(id);
            rs.setState("Ok");
            return rs;
        }catch(Exception e ){
            rs.setState("Non");
            return rs;
        }
    }

    @GetMapping("/one/{id}")

    public Reclamation getOne(@PathVariable String id){
        return reclamationRespository.find_id(id);
    }


    @GetMapping("/allreclamantion/{id}")
    public List<Reclamation> allReclamation(@PathVariable String id){
        return reclamationRespository.findbyClient(id);
    }

    @RequestMapping(value="/sendMail",method= RequestMethod.POST)
    public String sendMail(@RequestBody Mail mail){
        System.out.println("Spring Mail - Sending Simple Email with JavaMailSender Example");
        mail.setFrom("landolsii123@gmail.com");
        mail.setTo(mail.getTo());
        mail.setSubject("Reponse de votre reclamation");
        mail.setContent(mail.getContent());
        mailService.sendSimpleMessage(mail);
        return "ok";
    }

    @GetMapping("/setResponse/{idResponse}")
    public Reclamation changeResponded(@PathVariable String idResponse){
        Reclamation reclamation=reclamationRespository.find_id(idResponse);
        reclamation.setResponded("1");

        return reclamationRespository.save(reclamation);
    }

}
