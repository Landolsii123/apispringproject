package com.example.demo.Controller;

import com.example.demo.Dao.PersonneRepository;
import com.example.demo.Model.Personne;
import com.example.demo.Model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/Personne")
@CrossOrigin("*")
public class PersonneRestController {
@Autowired
    private PersonneRepository personneRepository;


@GetMapping("/all")
    public List<Personne> findAll(){
        return personneRepository.findAll();
    }

    @PostMapping("/add")

    public Personne addPersonne(@RequestBody Personne p){
    p.setRole("Admin");
    return personneRepository.save(p);
    }

    @PutMapping("/update/{id}")

    public Personne updatePersonne(@RequestBody Personne p,@PathVariable String id){
    p.setId(id);
    p.setRole("Admin");
    return personneRepository.save(p);
    }

    @DeleteMapping("/delete/{id}")

    public Response deletePersonne (@PathVariable String id){
    Response rs=new Response();
    try{
        personneRepository.deleteById(id);
        rs.setState("Ok");
        return rs;
    }catch(Exception e ){
        rs.setState("Non");
        return rs;
    }
    }

    @GetMapping("/one/{id}")

    public Personne getOne(@PathVariable String id){
        return personneRepository.find_id(id);
    }

    @PostMapping("/login")
    public HashMap<String,Personne> login(@RequestBody Personne p){

    HashMap<String,Personne> hp = new HashMap<>();

    try {
        hp.put("data",personneRepository.login(p.getLogin(),p.getPassword()));
    }
    catch(Exception e){
        hp.put("data",null);
    }
    return hp;
    }

}

