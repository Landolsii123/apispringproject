package com.example.demo.Controller;

import com.example.demo.Dao.ClientRepository;
import com.example.demo.Dao.PersonneRepository;
import com.example.demo.Model.Client;
import com.example.demo.Model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/Client")
@CrossOrigin("*")
public class ClientRestController {

        @Autowired
        private ClientRepository clientRepository;

        @Autowired
        private PersonneRepository personneRepository;


        @GetMapping("/all")
        public List<Client> findAll(){
            return clientRepository.findAll();
        }

        @PostMapping("/add")
        public Client addClient(@RequestBody Client c){
            c.setRole("Client");
            clientRepository.save(c);
            personneRepository.save(c);
            return c;
        }

        @PutMapping("/update/{id}")

        public Client updateClient(@RequestBody Client c,@PathVariable String id){
            c.setId(id);
            c.setRole("Client");

            return clientRepository.save(c);
        }

        @DeleteMapping("/delete/{id}")

        public Response deleteClient (@PathVariable String id){
            Response rs=new Response();
            try{
                clientRepository.deleteById(id);
                rs.setState("Ok");
                return rs;
            }catch(Exception e ){
                rs.setState("Non");
                return rs;
            }
        }

        @GetMapping("/one/{id}")

        public Client getOne(@PathVariable String id){
            return clientRepository.find_id(id);
        }

}
