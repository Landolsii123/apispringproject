package com.example.demo.Controller;

import com.example.demo.Dao.AnnonceRepository;
import com.example.demo.Dao.CategoryRepository;
import com.example.demo.Dao.PersonneRepository;
import com.example.demo.Model.Annonce;
import com.example.demo.Model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Annonce")
@CrossOrigin("*")
public class AnnonceController {
    @Autowired
    private AnnonceRepository annonceRepository;
    @Autowired
    private PersonneRepository personneRepository;
    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/all")
    public List<Annonce> findAll(){
        return annonceRepository.findAll();
    }

    @PostMapping("/add/{idP}/{idS}")
    public Annonce addAnnonce(@RequestBody Annonce a, @PathVariable String idP, @PathVariable String idS){
        a.setCategory(categoryRepository.find_id(idS));
        a.setPersonne(personneRepository.find_id(idP));
        return annonceRepository.save(a);
    }

    @PutMapping("/update/{id}/{idA}/{idS}")
    public Annonce updateAnnonce(@RequestBody Annonce a, @PathVariable String id, @PathVariable String idA, @PathVariable String idS){
        a.setId(id);
        a.setCategory(categoryRepository.find_id(idS));
        a.setPersonne(personneRepository.find_id(idA));
        return annonceRepository.save(a);
    }

    @DeleteMapping("/delete/{id}")
    public Response deleteAnnonce (@PathVariable String id ){
        Response rs= new Response();
        try {
            annonceRepository.deleteById(id);
            rs.setState("okay");
            return rs;
        }catch(Exception e){
            rs.setState("non");
            return rs;
        }
    }

    @GetMapping("/one/{id}")
    public Annonce getOne(@PathVariable String id){
        return annonceRepository.find_id(id);
    }


    @GetMapping("/allannonce/{id}")
    public List<Annonce> allAnnonce (@PathVariable String id){
        return annonceRepository.findbyAnnonceur(id);
    }

    @GetMapping("/allSousCategory/{id}")
    public List<Annonce> allAnnoncebyCategory(@PathVariable String id){
        return annonceRepository.findbySousCategory(id);
    }
}
