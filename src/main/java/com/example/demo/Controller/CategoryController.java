package com.example.demo.Controller;

import com.example.demo.Dao.CategoryRepository;
import com.example.demo.Model.Category;
import com.example.demo.Model.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/Categorie")
@CrossOrigin("*")
public class CategoryController {
    @Autowired
    private CategoryRepository categoryRepository;

    @GetMapping("/all")
    public List<Category> findAll(){
        return categoryRepository.findAll();
    }

    @PostMapping("/add")
    public Category addCategory(@RequestBody Category c){
        return categoryRepository.save(c);
    }

    @PutMapping("/update/{id}")
    public Category updateCategory(@RequestBody Category r, @PathVariable String id){
        r.setId(id);
        return categoryRepository.save(r);
    }

    @DeleteMapping("/delete/{id}")
    public Response deteleReservation (@PathVariable String id ){
        Response rs= new Response();
        try {
            categoryRepository.deleteById(id);
            rs.setState("okay");
            return rs;
        }catch(Exception e){
            rs.setState("non");
            return rs;
        }
    }

    @GetMapping("/one/{id}")
    public Category getOne(@PathVariable String id){
        return categoryRepository.find_id(id);
    }

}