package com.example.demo.Dao;

import com.example.demo.Model.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ClientRepository extends MongoRepository<Client,String> {
    @Query("{'id':?0}")
    Client find_id(String id);
}
