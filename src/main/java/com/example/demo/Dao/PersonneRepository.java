package com.example.demo.Dao;

import com.example.demo.Model.Personne;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface PersonneRepository extends MongoRepository<Personne,String>{
    @Query("{'id':?0}")
    Personne find_id(String id);

    @Query("{'login': ?0, 'password': ?1}")
    Personne login(String login, String password);

}
