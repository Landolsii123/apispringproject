package com.example.demo.Dao;

import com.example.demo.Model.Reclamation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface ReclamationRespository extends MongoRepository<Reclamation,String> {
    @Query("{'id':?0}")
    Reclamation find_id(String id);

    @Query("{'client.id':?0}")
    List<Reclamation> findbyClient(String id);

}
