package com.example.demo.Dao;

import com.example.demo.Model.Annonce;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface AnnonceRepository extends MongoRepository<Annonce,String>{
    @Query("{'id':?0}")
    Annonce find_id(String id);

    @Query("{'personne.id':?0}")
    List<Annonce> findbyAnnonceur(String id);

    @Query("{'category.id':?0}")
    List<Annonce> findbySousCategory(String id);
}
