package com.example.demo.Dao;


import com.example.demo.Model.Reservation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface ReservationRepository extends MongoRepository<Reservation,String>{
    @Query("{'id':?0}")
    Reservation find_id(String id);

    @Query("{'client.id':?0}")
    List<Reservation> findbyClient(String id);
}
